import Link from 'next/link';

const Newsletter = () => {
    return (
        <div className="news container">
            <h1 className="news-title mai">Dispatches From the March</h1>
            <p className="news-desc prev">Travel beneath the brightest skies or scale the heights of the darkest towers! Sign up for daring adventures full of warriors, sorcerers, and thieves as we wander into dragon-filled lands full of magic and excitement! Receive updates on my new Epic Fantasy and Sword and Sorcery releases, stay current on blogs and events, and access to new Fantasy eBooks for free every quarter along with newsletter-exclusive content. The quest starts here when you join Dispatches From The March and get two eBooks free upon registration!</p>
            <div className="cta-container">
                <a href="https://www.subscribepage.com/booksforthemarchprime" className="link link-brown cta">ONWARD</a>
            </div>
        </div>
    )
}

export default Newsletter;