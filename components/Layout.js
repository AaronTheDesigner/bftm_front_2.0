import Head from 'next/head';
import Footer from './Footer';
import NavBar from './NavBar';
import MobileNav from './MobileNav';


// (function(m,a,i,l,e,r){ m['MailerLiteObject']=e;function f(){
//     var c={ a:arguments,q:[]};var r=this.push(c);return "number"!=typeof r?r:f.bind(c.q);}
//     f.q=f.q||[];m[e]=m[e]||f.bind(f.q);m[e].q=m[e].q||f.q;r=a.createElement(i);
//     var _=a.getElementsByTagName(i)[0];r.async=1;r.src=l+'?v'+(~~(new Date().getTime()/1000000));
//     _.parentNode.insertBefore(r,_);})(window, document, 'script', 'https://static.mailerlite.com/js/universal.js', 'ml');

const Layout = props => {

    return (
        <div>
            <Head>
                <title>Jay Requard: Books For The March</title>
                <script src="https://static.mailerlite.com/js/w/webforms.min.js?v42b571e293fbe042bc115150134382c9" type="text/javascript"></script>
                <link rel="preconnect" href="https://fonts.googleapis.com"></link>
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin></link>
                <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@600&display=swap" rel="stylesheet"></link>
                
            </Head>
            <NavBar />
                <main>
                    {props.children}
                </main> 
            <MobileNav />
            <Footer />     
    </div>

    )
   
}

export default Layout;