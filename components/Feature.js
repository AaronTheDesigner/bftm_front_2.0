import Link from 'next/link';

const Feature = (props) => {
    //const author = authors[0];
    console.log(props.author.cover_image)


    return (
        <div className="container"style={{
            backgroundImage: `radial-gradient(rgba(255, 255, 255, 0.75) 10%, rgba(255, 255, 255, 0.8) 40%, rgba(255, 255, 255, 0.85) 50%, rgb(211, 221, 223, 1) 85%), url('${props.author.cover_image}')`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',

            }}>
            
            <div className="feature" >             
                <div className="feature-image">
                    <img src={props.feature_image} alt={props.title}/>
                </div>                
                <div className="feature-info">
                    <h1 className="card-title mai">{props.title}</h1>
                    <span className="feature-author">
                        <p className="mai">by {props.author.name}</p> <img src={props.author.profile_image} alt=""/>
                    </span>
                    <p className="card-content prev" >{props.custom_excerpt}</p>
                    <div className="feature-tags">
                        {props.tags.map(tag => {
                            return (
                                <span key={tag.slug}>{tag.name}</span>
                            )
                        })}
                    </div>
                    <div className="feature-links">
                    <Link href={{
                            pathname: `/post/${props.slug}`,
                            query: { slug: `${props.slug}`}
                    }}><a href="#" className="link link-red">More</a></Link>
                    <Link href={{
                        pathname: `/works`
                    }}><a href="#" className="link link-red">Works</a></Link>
                    </div>
                </div>
                
            </div>
        </div>
    )
}

export default Feature;