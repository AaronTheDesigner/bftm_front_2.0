
const MobileNav = () => {
    
    const toggle = (e) => {
        e.preventDefault();
        let nav = document.getElementById("nav");
        let toggle = nav.classList[1];
        

        if (toggle === "toggle-down") {
            nav.style.transform = "translate(0,0)";
            nav.className = "mobile-nav toggle-up";
        } else {
            nav.style.transform = "translate(0, 50%)";
            nav.className = "mobile-nav toggle-down";
        }
    }

    return (
        <nav className="mobile-nav toggle-down" id="nav">
            <div className="image-container">
                <img 
                    onTouchEnd={toggle}
                src="/earthlogo.svg" alt=""/>           
            </div>
            <ul className="mobile-links">
                <li>
                    <a href="/">
                        {/* <img src="/home.svg" className="icon" alt=""/> */}
                        <p className="mai">Home</p>
                    </a>
                </li>
                <li>
                    <a href="/about">
                        {/* <img src="/about.svg" className="icon" alt=""/> */}
                        <p className="mai">About</p>
                    </a>
                </li>
                <li>
                    <a href="/blog">
                        {/* <img src="/blog.svg" className="icon" alt=""/> */}
                        <p className="mai">Blog</p>
                    </a>
                </li>
                <li>
                    <a href="/works">
                        {/* <img src="/works.svg" className="icon" alt=""/> */}
                        <p className="mai">Works</p>
                    </a>
                </li>
                <li>
                    <a href="https://www.subscribepage.com/booksforthemarchprime">
                        {/* <img src="/newsletter.svg" className="icon" alt=""/> */}
                        <p className="mai">Letter</p>
                    </a>
                </li>
            </ul>
        </nav>
    )

}

export default MobileNav;