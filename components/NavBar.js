
const NavBar = () => {

    return (
        
        <nav className="nav mai">
            <div className="brand">
                <div className="logo-container">
                    <a href="/" id="toggle-nav">
                        <img className="logo" src="/earthlogo.svg" alt="Books For The March"/>
                        <img src="" alt=""/>
                    </a>
                </div>
                <span>
                    <a href="/"><h2>Jay Requard</h2></a>                
                </span>            
            </div>
            
            
            <ul className="nav-links">
                <li>
                    <a href="/" >                    
                        Home
                    </a>
                </li>
                <li>
                    <a href="/about">                    
                        About
                    </a>
                </li>
                <li>
                    <a href="/blog">                    
                    Blog
                    </a>
                </li>
                <li>
                    <a href="/works">
                    Works
                    </a>
                </li>
                <li>
                    <a href="https://www.subscribepage.com/booksforthemarchprime">
                    Newsletter
                    </a>
                </li>
            </ul>
        </nav>
    )

}

export default NavBar;