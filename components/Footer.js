
const Footer = () => (
    <footer className="footer" >
        <div className="footer-top">
            <div className="footer-left" >
                <div className="footer-social">
                    <div className="box">
                        <a href="https://www.facebook.com/JayRequardOfficial"><img src="/Facebook.svg" alt="" /></a>
                        <a href="https://twitter.com/JayRequard"><img src="/Twitter.svg" alt="" /></a>
                        <a href="https://www.instagram.com/jayrequard/"><img src="/Instagram.svg" alt="" /></a>
                    </div>                
                </div>
                <div className="footer-logo">
                    <img src="/earthlogo.svg" alt="" />
                </div>
            </div>
            <div className="footer-right">
            <div className="footer-map">
                <ul>
                    <li>
                        <a href="/" >                    
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="/about">                    
                            About
                        </a>
                    </li>
                    <li>
                        <a href="/blog">                    
                        Blog
                        </a>
                    </li>
                    <li>
                        <a href="/works">
                        Works
                        </a>
                    </li>
                    <li>
                        <a href="https://www.subscribepage.com/booksforthemarchprime">
                        Newsletter
                        </a>
                    </li>
                </ul>
                <hr />
                </div>
                <div className="footer-legal">
                    <p>&copy; Copyright Jay Requard 2021</p>
                    <p>All rights reserved. Built by Aaron Toliver</p>
                </div>
                <div className="footer-build">
                    <img src="/nextjs.svg" alt="" />
                    <img src="/mongodb.svg" alt="" />
                    <img src="/nodejs.svg" alt="" />
                    <img src="/sass.svg" alt="" />
                </div>
            </div>
        </div>
        <div className="footer-admin">
        <a href="https://bftm-admin.herokuapp.com/ghost/#/site">admin</a>
        </div>        
        <div className="footer-bottom">

        </div>
    </footer>    
)

export default Footer;