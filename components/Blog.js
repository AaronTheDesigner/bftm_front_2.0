import Link from 'next/link';

const Blog = (props) => {

    console.log(props.author.profile_image)
    return (
        <div className="blog" style={{
            backgroundImage: `url(${props.feature_image})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundAttachment: 'fixed',
            backgroundColor: 'grey',
            backgroundBlendMode: 'multiply'            
        }}>
            <div>
                <h1 className>{props.header}</h1>
                <div className="blog-header">
                    <h1 className="blg-title mai">{props.title}</h1>
                </div>
                <div className="blog-author mai">
                    <span>By {props.author.name} <img src={props.author.profile_image} alt="" /></span>
                </div>
                <div className="blog-info crimson">
                    <p className="card-content">{props.custom_excerpt}</p>
                </div>
                <div className="blog-tags">
                    {props.tags.map(tag => {
                        return (
                            <span key={tag.slug}>{tag.name}</span>
                        )
                    })}
                </div>
                <div className="blog-links">
                    <Link href={{
                        pathname: `/post/${props.slug}`,
                        query: { slug: `${props.slug}`}
                    }}><a href="#" className="link link-blue">Read</a></Link>
                    <Link href={{
                        pathname: `/blog`
                    }}><a href="#" className="link link-blue">Blog</a></Link>
                </div>
            </div>
        </div>
    )
}

export default Blog;