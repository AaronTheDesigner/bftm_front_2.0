import '../styles/style.css'

const URL = process.env.API_URL;
const KEY = process.env.CONTENT_API_KEY;

function MyApp({ Component, pageProps, posts, works, features, tags, authors, allItems }) {
  return <Component {...pageProps} posts={posts} works={works} features={features} tags={tags} authors={authors} allItems={allItems} />
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
  const blog = await fetch(`${URL}/ghost/api/v3/content/posts/?key=${KEY}&filter=tag:blog&include=tags,authors`);
  const work = await fetch(`${URL}/ghost/api/v3/content/posts/?key=${KEY}&filter=tag:works&include=tags,authors`);
  const feature = await fetch(`${URL}/ghost/api/v3/content/posts/?key=${KEY}&filter=featured:true&include=tags,authors`);
  const tag = await fetch(`${URL}/ghost/api/v3/content/tags/?key=${KEY}`);
  const author = await fetch(`${URL}/ghost/api/v3/content/authors/?key=${KEY}`);
  const all = await fetch(`${URL}/ghost/api/v3/content/posts/?key=${KEY}&include=tags,authors`);

  const blogData = await blog.json();
  const workData = await work.json()
  const featureData = await feature.json();
  const tagData = await tag.json();
  const authorData = await author.json();
  const allData = await all.json();
  
  const posts = blogData.posts;
  const works = workData.posts;
  const features = featureData.posts;
  const tags = tagData.tags;
  const authors = authorData.authors;
  const allItems = allData.posts;
  
  let pageProps = {};
  if(Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  return { pageProps, posts, works, features, tags, authors, allItems, revalidate: 60000, fallback: false }
}

export default MyApp