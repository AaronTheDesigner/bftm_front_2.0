import Link from 'next/link';
import Layout from '../components/Layout';

const WorkView = (props) => {

    const tag = props.tags.filter(tag => {
      return tag.slug === 'works'
    })

    return (
      <Layout>
      <div className="container pt-60">
      <div className="page-header">
          <h1 className="mai">WORKS</h1>
        </div>
        <div className="grid">
          {props.works.map(work => {
            return (
            <div className="card card-work" key={work.id}>
              <div className="card-image">
                <img src={work.feature_image} alt=""/>
              </div>
              <div className="card-info">
                <h1 className="card-title mai">{work.title}</h1>
                <span className="card-author"> by {work.primary_author.name} 
                <img src={work.primary_author.profile_image} alt=""/>
                </span>
                <p className="card-intro prev">{work.custom_excerpt}</p>
                <span className="card-tags tag-red">{work.tags.map(tag => {
                    return <span key={tag.id} >{tag.name}</span>
                  })}
                </span>
                
                <div className="card-links">
                <Link href={{
                        pathname: `/post/${work.slug}`,
                        query: { slug: `${work.slug}`}
                    }}><a href="#" className="link link-red">Read</a></Link>
                </div>
              </div>
            </div>
            )
          })}
        </div>
      </div>

      
    </Layout>


    )
}

export default WorkView;