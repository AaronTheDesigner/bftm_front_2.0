import Layout from '../../components/Layout';
import { useRouter } from 'next/router';

const Post = (props) => {
    const router = useRouter();
    const slug = router.query.slug
    const filter = props.allItems.filter(item => {
        return item.slug === slug;
    })


    const post = filter[0]
    const author = post.primary_author;
    const text = post.html;
    console.log(post.published_at);
    console.log(post.reading_time);

    const returnTag = () => {
       const tags = post.tags;
       const result = tags.find(({name}) => name === "Works" || "Blog")
       return result;
    }

    const renderHeader = () => {
        const tag = returnTag();
        

        if (tag.name === "Works") {
            return (
                <div className="container pt-60">
                    <div className="work">
                        <div className="work-image">
                            <img src={post.feature_image} alt=""/>
                        </div>
                        <div className="work-info">
                            <h1 className="work-title mai" >{post.title}</h1>
                            <span className="mai"> <p>by {author.name}</p>  <img src={author.profile_image} alt=""/></span>
                            <p className="work-intro">{post.custom_excerpt}</p>
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div className="blog" style={{
                backgroundImage: `url(${post.feature_image})`,
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                backgroundAttachment: 'fixed',
                backgroundColor: 'grey',
                backgroundBlendMode: 'multiply'
            }}>
                
                    <div className="blog-info">
                        <h1 className="blog-title mai" >{post.title}</h1>
                        <span className="mai"> <p>by {author.name}</p>  <img src={author.profile_image} alt=""/></span>
                        <p className="blog-intro">{post.custom_excerpt}</p>
                    </div>
                

            </div>
        )

    }
    
    return (
        <Layout>
        
            <div>
                <div className="container">
                    {renderHeader()}                
                    <main className="main">
                        <div className='content-container' >
                            <div className="content" dangerouslySetInnerHTML={{__html: text}} />
                        </div>
                    </main>
                </div>
            </div>
        </Layout>
    )
}

export default Post