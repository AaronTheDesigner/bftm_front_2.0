import Layout from '../components/Layout';
import Blog from '../components/Blog';
import Feature from '../components/Feature';
import Newsletter from '../components/Newsletter';
import Hero from '../components/Hero';
import Link from 'next/link';


const Index = (props) => {
const {features, posts, authors, tags} = props;
const feature = features[0];
const post = posts[0];
const author = authors[0];
  
  return (
    <Layout>
      <div className="home">
        <Hero/>
      </div>
      
        <div className="container" id="main" style={{
          backgroundImage: `radial-gradient(rgba(255, 255, 255, 0.75) 10%, rgba(255, 255, 255, 0.8) 40%, rgba(255, 255, 255, 0.85) 50%, rgb(211, 221, 223, 1) 85%), url('${author.cover_image}')`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',

        }} >
        
          <div className="author" >
            <div className="author-image">
              <img src={author.profile_image} alt="Jay Requard"/>
            </div>
            <div className="author-info">
            
              <h1 className="card-title mai">Jay Requard</h1>
              <p className="card-content prev">{author.bio}</p>
              <hr/>
              <div className="author-links">
                <Link href="/about"><a href="#" className="link link-blue">More</a></Link>          
              </div>
              <div className="author-awards">
              <div className="footer-social">
                    <div className="box">
                        <a href="https://www.facebook.com/JayRequardOfficial"><img src="/Facebook.svg" alt="" /></a>
                        <a href="https://twitter.com/JayRequard"><img src="/Twitter.svg" alt="" /></a>
                        <a href="https://www.instagram.com/jayrequard/"><img src="/Instagram.svg" alt="" /></a>
                    </div>                
                </div>
            </div>              
            </div>         
          </div>
        </div>
        <Blog
          key={post.id}
          title={post.title}
          custom_excerpt={post.custom_excerpt}
          feature_image={post.feature_image}
          slug={post.slug}
          tags={post.tags}
          author={post.primary_author}
        /> 
        <Feature
          key={feature.id}
          title={feature.title}
          custom_excerpt={feature.custom_excerpt}
          feature_image={feature.feature_image}
          slug={feature.slug}
          tags={feature.tags}
          author={feature.primary_author}
        />  
        <Newsletter/>
        
    </Layout>
  )
}

export default Index;
