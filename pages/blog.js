import Layout from '../components/Layout';
import Link from 'next/link';

const BlogView = (props) => {

  const tag = props.tags.filter(tag => {
    return tag.slug === 'blog'
  });

  return (  
    <Layout>
      <div className="container pt-60">
        <div className="page-header">
          <h1 className="mai">BLOG</h1>
        </div>
        <p className="page-description">{tag[0].description}</p>
        <div className="grid">
          {props.posts.map(post => {
            return (
            <div className="card card-blog" key={post.id}>
              <div className="card-image">
                <img src={post.feature_image} alt=""/>
              </div>
              <div className="card-info">
                <h1 className="card-title mai">{post.title}</h1>
                <span className="card-author mai"> by {post.primary_author.name} 
                  <img src={post.primary_author.profile_image} alt=""/>
                </span>
                <p className="card-intro">{post.custom_excerpt}</p>
                <div className="card-tags tag-blue">{post.tags.map(tag => {
                    return <span key={tag.id} >{tag.name}</span>
                  })}
                </div>                
                <div className="card-links">
                <Link href={{
                        pathname: `/post/${post.slug}`,
                        query: { slug: `${post.slug}`}
                    }}><a href="#" className="link link-blue">Read</a></Link>
                </div>
              </div>
            </div>
            )
          })}
        </div>
      </div>

      
    </Layout>

    )
}

export default BlogView;
