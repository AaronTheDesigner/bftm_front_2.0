import Layout from '../components/Layout';
import Hero from '../components/Hero';


const AboutView = (props) => {
    const author = props.authors[0];
    return (
        <Layout>
            <div className="mt-60" style={{
                backgroundImage: `url(${author.cover_image})`,
                backgroundRepeat: 'none',
                backgroundSize: 'cover',
                backgroundAttachment: 'fixed',
                backgroundPositionX: '50%',
                maxHeight: '230vh',
                backgroundColor: 'grey',
                backgroundBlendMode: 'multiply'
            }}>
                <div className="container">
                    <div className="panel">
                        <div className="panel-image">
                            <img src={author.profile_image} alt=""/>
                        </div>
                        <div className="panel-info">
                            <h1 className="mai">{author.name}</h1>
                            <p> 
                                Risen from the hills of North Carolina, Jay Requard is an award-winning fantasy author known for
                                “heroes with genuine emotions and concerns for the word they live in” facing off against the
                                darkest powers with little more than love, courage, steel, and magic to set things right. From the pulse-
                                pounding heists of Thief of Destiny to the dark desert romance of Death &amp; Dust and other tales of wonder,
                                readers have noted his modern take on the genre feels like “diving into new hero mythology.”
                            </p>
                            <p>
                                Featured in Amazing Stories, Charlotte Geeks, Grind Pulp, and BULLSPEC, Jay received the Write Well
                                Award in 2016 for his sword &amp; sorcery epic, Mask of the Kravyads. He is also the runner-up for the
                                Manly Wade Wellman Award for Best Fiction for War Pigs. A graduate of the University of North
                                Carolina at Charlotte, he holds degrees in history and spirituality.
                            </p>
                            <p>
                                After leaving the biomedical and legal industries, as well as helping to establish two independent genre
                                publishers, Jay currently lives in New York City with his beautiful wife, a wonderful son, and a
                                rambunctious shadow cat named Mona Underfoot. When not with his family or writing he enjoys
                                meditating, reading, martial arts, cooking, and or course, wandering off for the next adventure.
                            </p>
                                 
                        </div>                
                    </div> 
                </div>           
            </div>

        </Layout>
    )
} 

export default AboutView;